# Nginx frontend for Icecast
class icecast2::nginx(
  Enum['present','absent'] $ensure = 'present',
  Stdlib::Host $hostname = $icecast2::hostname,
  Stdlib::Unixpath $ssl_cert = "/etc/letsencrypt/live/${hostname}/fullchain.pem",
  Stdlib::Unixpath $ssl_key = "/etc/letsencrypt/live/${hostname}/privkey.pem",
  Optional[Variant[Array[String], String]] $raw_prepend = undef,
  Optional[Variant[Array[String], String]] $raw_append = undef,
  Optional[Variant[Array[String], String]] $location_raw_prepend = undef,
  Optional[Variant[Array[String], String]] $location_raw_append = undef,
) {
  include nginx
  include icecast2
  nginx::resource::server { $hostname:
    proxy                => 'http://localhost:8000',
    ssl                  => true,
    ssl_redirect         => true,
    ssl_cert             => $ssl_cert,
    ssl_key              => $ssl_key,
    raw_prepend          => $raw_prepend,
    raw_append           => $raw_append,
    location_raw_prepend => $location_raw_prepend,
    location_raw_append  => $location_raw_append,
  }
}
