# configure an icecast2 server
#
# @param hostname the FQDN the service will be visible as to the public
#
# @param bind_address which address to listen on
#
# @param admin_password administrator password, can kick clients and
# see everything
#
# @param relay_password relay password, can setup relay points
#
# @param source_password source password, can be used to create new
# mountpoints by sources
#
# @param clients how many clients are allowed
#
# @param sources how many sources are allowed
class icecast2(
  Stdlib::Host $hostname,
  Stdlib::IP::Address $bind_address,
  String[6] $admin_password,
  String[6] $relay_password,
  String[6] $source_password,
  Integer $clients,
  Integer $sources,
  Enum['present','absent'] $ensure = 'present',
  String $extra_config = '',
) {
  $ensure_service = $ensure ? {
    'present' => 'running',
    'absent' => 'stopped',
  }
  package { 'icecast2':
    ensure => $ensure,
  }
  -> file { '/etc/icecast2/icecast.xml':
    ensure  => $ensure,
    mode    => '0440',
    owner   => 'icecast2',
    group   => 'icecast',
    notify  => Service['icecast2'],
    content => epp('icecast2/icecast.xml.epp', {
      'hostname'        => $hostname,
      'bind_address'    => $bind_address,
      'admin_password'  => $admin_password,
      'relay_password'  => $relay_password,
      'source_password' => $source_password,
      'clients'         => $clients,
      'sources'         => $sources,
      'extra_config'    => $extra_config,
      }),
  }
  -> service { 'icecast2':
    ensure => $ensure_service,
  }
}
